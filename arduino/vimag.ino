/* programme de gestion d'un capteur PIR et de tags RFID
 * 
 * 
 * CAPTEUR PIR
 * 
 * PIR         UNO
 * VCC         5V                       
 * OUT         D2
 * GND         GND
 * repère les fronts montants du capteur et note la date dans un buffer de taille bufferPIR
 * calcule le niveauPIR = nombre de fronts récents(max 15), délai inférieur à dureemaxPIR
 * envoie le niveauPIR sur le port série s'il a changé
 *  
 * CAPTEUR RC522
 * 
 * RC522       UNO
 * RST/Reset    9             
 * SPI SS      10            
 * SPI MOSI    11 
 * SPI MISO    12 
 * SPI SCK     13
 * 
 * détecte le passage d'un tag RFID et envoie son id en hexadécimal  
 * 
 * envoie les données captées sous la forme : 
 * PIR : 
 * P,<niveau 0-15>
 * T,<ID en Hexadecimal>
*/

#include <SPI.h> //librairie pour communication via le protocole SPI 
#include <MFRC522.h> //librairie de gestion du RC522

const int brochePIR = 2;
const unsigned long dureeMaxPIR = 60000; //durée max de 60s pendant laquelle les détections sont prises en compte
const int bufferPIR = 15; //taille du buffer montantPIR
int etatPIR; //état courant du capteur PIR
unsigned long montantPIR[bufferPIR] = {0L}; //tableau des dates des fronts montants du capteur PIR
int montantIdx = 0; //index dans le tableau
int niveauPIR = 0; //niveau d'activité mesuré par le capteur PIR

const int brocheSPISS = 10;
const int brocheRST = 8;
MFRC522 rfid(brocheSPISS, brocheRST); //initalisation du module RFID

void printHex(byte *buffer, byte bufferSize) { //fonction pour écrire l'id du tag RFID en héxadécimal (car plus compact)
  for (byte i = 0; i < bufferSize; i++) {
    if (buffer[i] < 0x10) { // si le nombre est inférieur à 0x10 = 16
      Serial.print("0"); // on ajoute un zero pour avoir toujours deux chiffres
    }
    Serial.print(buffer[i], HEX);
  }
}

void setup() {
  // initialisation

  Serial.begin(115200); // initialise la liaison série à 115200 bauds

  pinMode(brochePIR,INPUT); // parametre la broche du capteur PIR en entrée
  etatPIR = 0;

  SPI.begin(); // Initialisation du bus SPI
  rfid.PCD_Init(); // Initialisation du capteur RFID-RC522 

}

void loop() {
  // boucle principale

  int lecturePIR = digitalRead(brochePIR); //lit le capteur PIR

  // test de front montant
  if ((etatPIR == 0) && (lecturePIR == 1)) {
    // nouveau front montant à ajouter dans le tableau montantPIR
    montantPIR[montantIdx] = millis();
    montantIdx = (montantIdx +1) % 15; //calcul de l'index
  }
  etatPIR = lecturePIR; //mise à jour de l'état courant du PIR
  
  //calcul du niveau : nombre de front montant datant de moins de duréeMaxPIR
  unsigned long d = millis();
  int niveau = 0;
  for (int i = 0;i<bufferPIR;i++) {
    if ((montantPIR[i] !=0) && (d - montantPIR[i] < dureeMaxPIR)) {
      niveau++;
    }
  }
  
  if (niveau != niveauPIR) { // si le niveau a évolué
    niveauPIR = niveau; //mise à jour du niveau PIR
    Serial.print("P,"); // envoi du niveau sur la liaison série
    Serial.println(niveauPIR);
  }

  if (rfid.PICC_IsNewCardPresent() && rfid.PICC_ReadCardSerial()) {
    // si une nouvelle carte est détectée 
    Serial.print("T,"); //envoi de son id sur la liaison série
    printHex(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();

    rfid.PICC_HaltA();
    rfid.PCD_StopCrypto1();
  }

  delay(50);
}
